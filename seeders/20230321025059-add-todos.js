'use strict'

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [
      {
        title: 'Belanja',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Bekerja',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Belajar',
        status: 'active',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {})
  },
}
