const app = require('../app')
const request = require('supertest')

describe('API /todos', () => {
  it('test get /todos', (done) => {
    request(app)
      .get('/todos')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        const firstData = response.body[0]
        expect(firstData.id).toBe(1)
        expect(firstData.title).toBe('Belanja')
        expect(firstData.status).toBe('active')
        done()
      })
      .catch(done)
  })

  it('test get /todos/:id', (done) => {
    request(app)
      .get('/todos/3')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.id).toBe(3)
        expect(response.body.title).toBe('Belajar')
        expect(response.body.status).toBe('active')
        done()
      })
      .catch(done)
  })

  it('test post /todos', (done) => {
    request(app)
      .post('/todos')
      .send({ title: 'Berlatih', status: 'active' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .then((response) => {
        const data = response.body
        expect(data).toHaveProperty('id')
        expect(data.title).toBe('Berlatih')
        expect(data.status).toBe('active')
        done()
      })
      .catch(done)
  })

  it('test delete /todos/:id', (done) => {
    request(app)
      .delete('/todos/2')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        const data = response.body
        expect(data.message).toBe('Deleted Successfully')
        done()
      })
      .catch(done)
  })
})
